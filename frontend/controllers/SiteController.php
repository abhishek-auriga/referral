<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

use common\models\User;
use common\models\Organization;
use common\models\Position;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                //\Yii::$app->user->login($user, 3600 * 24 * 30);
                //return $this->goHome();
                if ($login = Yii::$app->getUser()->login($user, 3600 * 24 * 30)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

        /**
     * Displays about page.
     *
     * @return mixed
     */
    /*public function actionRefrrl()
    {
        Yii::$app->tools->display("dummy content in component"); exit;
        //echo '<pre>'; print_r(Yii::$app->params); exit();
        if (empty(Yii::$app->params['Client_ID']) || Yii::$app->params['Client_ID'] === '' || Yii::$app->params['Client_Secret'] === '') {
         echo 'You need a API Key and Secret Key to test the sample code. Get one from <a href="https://www.linkedin.com/developer/apps/">https://www.linkedin.com/developer/apps/</a>';
         exit;
        } else {
            $model = new SignupForm();
            if(!empty(Yii::$app->request->get('code'))) {
                //$model = new SignupForm();
                //echo '<pre>'; print_r(Yii::$app->request->get('code')); exit;
                //Yii::$app->request->post('firstName');
                //echo '<pre>'; print_r(Yii::$app->request->post); exit;
                $response['username'] = 'test1';
                $response['email'] = 'dasd@gmail.com';
                $response['password'] = "123456";
                $response['firstName'] = 'test';
                //$response['lastName'] = 'user';
                //print_r($model->attributes);
                //$model->setAttributes($response);
                //$model->signup();
                
                //$model->load($response, '');
                //print_r($model); exit();
                echo "Step1";
                if ($model->load($response, '')) {
                    echo "Step2";
                //if ($model->setAttributes($response)) {
                    //$user = $model->signup();
                    //echo '<pre>';
                    //print_r($model->validate());
                    //print_r($model->errors); // same as print_r($model->getErrors());

                    //print_r($model->getErrorSummary(""));
                    //print_r($model); exit();
                    if($model->validate()) {
                        if ($user = $model->signup()) {
                            echo "Step3";
                            if (Yii::$app->getUser()->login($user, 3600 * 24 * 30)) {
                                return $this->goHome();
                            }
                        }
                    } else {
                        echo "Step4";
                        print_r($model->errors); // same as print_r($model->getErrors());
                        print_r($model->getErrorSummary(""));
                        //exit;
                    }
                }
                //exit;
            }

        }
        echo '<pre>';print_r($model);
        $model->addError('username',"testing add error!");
        print_r($model->errors); 
        print_r($model->getErrorSummary(""));
        exit();
        //return $this->render('login-button-bak');
        return $this->render('login-button', [
                    'model' => $model,
                ]);

    }*/
    public function actionRefrrl()
    {
        //Yii::$app->tools->display("dummy content in component"); exit;
        //echo '<pre>'; print_r(Yii::$app->params); exit();
        if (empty(Yii::$app->params['Client_ID']) || Yii::$app->params['Client_ID'] === '' || Yii::$app->params['Client_Secret'] === '') {
         echo 'You need a API Key and Secret Key to test the sample code. Get one from <a href="https://www.linkedin.com/developer/apps/">https://www.linkedin.com/developer/apps/</a>';
         exit;
        } else {
            $model = new SignupForm();
            if(!empty(Yii::$app->request->get('code'))) {

                $data = Yii::$app->tools->getAccessToken(Yii::$app->request->get('code'));
                if(isset($data['error']) && !empty($data['error'])) { // if invalid output error
                    $model->addError('username', 'Some error occured<br><br>' . $data['error_description'] . '<br><br>Please Try again.');
                } else {
                    $response = Yii::$app->tools->getUserData($data['access_token']);
                    if(isset($data['response']) && !empty($data['response'])) {
                        $model->addError('username', $data['response']);
                    } else {
                        //echo '<pre>'; print_r($response); exit;
                        $userData['username'] = $response['emailAddress'];
                        $userData['email'] = $response['emailAddress'];
                        $userData['password'] = "123456";
                        $userData['firstName'] = $response['firstName'];
                        $userData['lastName'] = $response['lastName'];
                        $userData['headline'] = $response['headline'];
                        $userData['industry'] = isset($response['industry']) ? $response['industry'] : Yii::$app->params['default_industry'];
                        $userData['pictureUrls'] = (isset($response['pictureUrls']['values']) && isset($response['pictureUrls']['values'][0])) ? $response['pictureUrls']['values'][0] : Yii::$app->params['default_picture_url'];
                        $userData['linkedinId'] = $response['id'];

                        if ($model->load($userData, '')) {
                            if ($model->validate()) {
                                $userModel = User::find()->where(['email' => $userData['email']])->one();
                                echo "<br>check user exists query<br>";
                                if (!$userModel) {
                                    // new user
                                    $user = $model->signup();
                                    $userId = $user->id;
                                    $isNewRecord = true;
                                    echo "<br>New user inserted<br>";
                                } else {
                                    // existing user
                                    $userId = $userModel->id;
                                    $userModel->load($userData, '');
                                    $userModel->save();
                                    $user = $userModel;
                                    echo "<br>update user<br>";
                                    //echo '<pre>'; print_r($userModel->attributes); exit();
                                }
                                //if ($user = $model->signup()) {
                                    //print_r($user->attributes);
                                    //print_r($user->id);
                                    //$userId = $user->id;
                                    
                                    // save organization
                                    $organizationId = 0;
                                    $modelOrg = new Organization();
                                    $data = [];
                                    //echo '<pre>';print_r($response['positions']); exit();
                                    if (isset($response['positions']['values'][0]['company']['name'])) {
                                        $posDetails = $response['positions']['values'][0];
                                        //echo '<pre>';print_r($response['positions']); exit();
                                        $data['name'] = $posDetails['company']['name'];
                                        $data['industry'] = isset($posDetails['company']['industry']) ? $posDetails['company']['industry'] : Yii::$app->params['default_organization_industry'];
                                        $data['linkedInOrgId'] = isset($posDetails['company']['id']) ? $posDetails['company']['id'] : Yii::$app->params['default_organization_id'];
                                        $modelOrg->load($data, '');
                                        echo "<br>organization data loaded<br>";
                                        if ($modelOrg->validate()) {
                                            echo "<br>organization data validated<br>";
                                            $checkExists = $modelOrg::find()->where(['name' => $data['name'], 'linkedInOrgId' => $data['linkedInOrgId']])->one();
                                            echo "<br>get organization data to check exists or not<br>";
                                            if ($checkExists) {
                                                $organizationId = $checkExists->id;
                                                echo "<br>organization already exist. ID: " . $organizationId. "<br>";
                                            } else {
                                                $modelOrg->save();
                                                $organizationId = $modelOrg->getPrimaryKey();
                                                echo "<br>New organization inserted<br>";
                                            }
                                        } else {
                                            //echo "<br>organization data error:<br>";
                                            //print_r($modelOrg->errors);
                                        }

                                        // save position
                                        if ($organizationId) {

                                            if (!isset($isNewRecord) || !$isNewRecord) {
                                                $positionData = $userModel->getPositions()->where(['isCurrent' => 1])->one();
                                                echo "<br>get current position with organization<br>";
                                                /*echo '<pre>';
                                                print_r($positionData->attributes);
                                                print_r($positionData->organization->attributes); exit();*/
                                                if($positionData) {
                                                    if ($data['linkedInOrgId'] != $positionData->organization->attributes['linkedInOrgId']) {
                                                        // make existing isCurrent = 0
                                                        $modelPosition = Position::findOne($positionData->attributes['id']);
                                                        $modelPosition->isCurrent = 0;
                                                        $modelPosition->save();
                                                        echo "<br>update existing position isCurrent = 0<br>";

                                                        //insert new record
                                                        $isNewRecord = true;
                                                    } else {
                                                        // match all existing values with new
                                                        //update position

                                                        //new data array
                                                        //$newData['userId'] = $userId;
                                                        $newData['organizationId'] = $organizationId;
                                                        //$newData['isCurrent'] = 1;
                                                        $newData['startMonth'] = isset($posDetails['startDate']['month']) ? $posDetails['startDate']['month'] : 0;
                                                        $newData['startYear'] = isset($posDetails['startDate']['year']) ? $posDetails['startDate']['year'] : 0;
                                                        $newData['positionTitle'] = isset($posDetails['title']) ? $posDetails['title'] : '';

                                                        //$existingData['userId'] = $userId;
                                                        $existingData['organizationId'] = $positionData->attributes['organizationId'];
                                                        //$existingData['isCurrent'] = 1;
                                                        $existingData['startMonth'] = $positionData->attributes['startMonth'];
                                                        $existingData['startYear'] = $positionData->attributes['startYear'];
                                                        $existingData['positionTitle'] = $positionData->attributes['positionTitle'];

                                                        $diff = array_diff($newData, $existingData);
                                                        if(!empty($diff)) {
                                                            $modelPosition = Position::findOne($positionData->attributes['id']);
                                                            //$modelPosition->load($newData);
                                                            /*$modelPosition->organizationId = $newData['organizationId'];
                                                            $modelPosition->startMonth = $newData['startMonth'];
                                                            $modelPosition->startYear = $newData['startYear'];
                                                            $modelPosition->positionTitle = $newData['positionTitle'];*/
                                                            $modelPosition->load($newData, '');
                                                            if($modelPosition->validate()) {
                                                                echo "<br>position updated<br>";
                                                                $modelPosition->save();
                                                            }
                                                        } else {
                                                           echo "<br>nothing to update in position<br>"; 
                                                        }
                                                    }
                                                }
                                            }

                                            if (isset($isNewRecord) && $isNewRecord) {
                                                echo "<br>New position inserted<br>";
                                                $modelPos = new Position();
                                                $data = [];
                                                $data['userId'] = $userId;
                                                $data['organizationId'] = $organizationId;
                                                $data['isCurrent'] = 1;
                                                $data['startMonth'] = isset($posDetails['startDate']['month']) ? $posDetails['startDate']['month'] : 0;
                                                $data['startYear'] = isset($posDetails['startDate']['year']) ? $posDetails['startDate']['year'] : 0;
                                                $data['positionTitle'] = isset($posDetails['title']) ? $posDetails['title'] : '';
                                                $modelPos->load($data, '');
                                                if($modelPos->validate()) {
                                                    $modelPos->save();
                                                }
                                            }   
                                        }
                                    }
                                    //die("end");
                                    // login user
                                    if (Yii::$app->getUser()->login($user, 3600 * 24 * 30)) {
                                        return $this->goHome();
                                    }

                                    // in case of login
                                    /*$modelLogin = new LoginForm();
                                    if ($modelLogin->load(['username' => 'abhishekf2k1@gmail.com', 'password' => '123456'], '') && $modelLogin->login()) {
                                        return $this->goHome();
                                    }*/
                                //}
                            } else {
                                //echo "Step4";
                                //print_r($model->errors); // same as print_r($model->getErrors());
                                //print_r($model->getErrorSummary(""));
                                //exit;
                            }
                        }
                    }
                }
            }

        }
        /*echo '<pre>';print_r($model);
        $model->addError('username',"add custom error!");
        print_r($model->errors); 
        print_r($model->getErrorSummary(""));
        exit();*/
        return $this->render('refrrl', [
                    'model' => $model,
                ]);

    }


    /**
     * this is testing action to learn basic of 
     * 1. save data
     * 2. check whether record already exists
     * 3. if record exists than get existing record else save new and get inserted id
     * 4. basic validation rules
     */
    public function actionSaveData() {
        echo '<pre>';
        $model = new Organization();
        $data = [];
        $data['name'] = 'auriga IT';
        $data['industry'] = 'IT software';
        $data['linkedInOrgId'] = 12312;
        $model->load($data, '');
        if($model->validate()) {

            //$checkExists = $model::find()->where(['name' => $data['name'], 'linkedInOrgId' => $data['linkedInOrgId']])->exists(); // will return boolean response
            $checkExists = $model::find()->where(['name' => $data['name'], 'linkedInOrgId' => $data['linkedInOrgId']])->one();
            if($checkExists) {
                print_r($checkExists->attributes);
                print_r($checkExists->name);
            } else {
                $model->save();
                echo "Primary Key: " . $model->getPrimaryKey() . "<br>";
            }
        }
        print_r($model->errors);
        exit;
    }

    /**
     * this is testing action to insert dummy data in positions table
     */
    public function actionPositionData() {
        echo '<pre>';
        /*$model = new Position();
        $data = [];
        $data['organizationId'] = 2;
        $data['isCurrent'] = 0;
        $data['startMonth'] = 3;
        $data['startYear'] = 2016;
        $data['positionTitle'] = "Software Engineer";
        $model->load($data, '');
        if($model->validate()) {
                $model->save();
                echo "Primary Key: " . $model->getPrimaryKey() . "<br>";
        }
        //print_r($model->errors);
        */

        //$modelPosition = Position::findOne(2); // dont use find(2)->one(), both are not same
        // ->one() can be used after where() clause
        $modelPosition = Position::find()->where(['organizationId' => 2])->one();
        $newData['organizationId'] = 2;
        $newData['isCurrent'] = 0;
        $newData['startMonth'] = 4;
        $newData['startYear'] = 2016;
        $newData['positionTitle'] = "Software Engineer";
        //$modelPosition->isCurrent = 1;
        $modelPosition->load($newData, '');
        //$modelPosition->setAttributes($newData);
        //print_r($modelPosition); exit();
        if($modelPosition->validate()) {
            echo "here";
            $modelPosition->save();
        }

        print_r($modelPosition->errors);
        exit;
    }

    /**
     * this action is to get relational data of user/positions/organizations
     */
    public function actionPositionList() {
        echo '<pre>';
        /*$userModel = User::find()->where(['email' => 'abhishekf2k1@gmail.com'])->one();
        //print_r($userModel);
        //print_r($userModel->positions);
        // all positions & respective organization list, using getPositions(in User model) & getOrganization(in Position model) relationsip
        foreach ($userModel->positions as $key => $value) {
            print_r($value->attributes);
            print_r($value->organization->attributes);
        }
        // all organizations list, using getOrganization(in User) relationsip
        foreach ($userModel->organization as $key => $value) {
            print_r($value->attributes);
        }*/

        // get current position & organization
        $userModel = User::find()->where(['email' => 'abhishekf2k1@gmail.com'])->one();
        $positionData = $userModel->getPositions()->where(['isCurrent' => 1])->one();
        if ($positionData) {
            print_r($positionData->attributes);
            print_r($positionData->organization->attributes);
        } else {
            echo "no data found!";
        }
        exit();
    }

    public function actionSuggestions() {
    //select u.id,u.username,c.userId,c.connectedUserId from user u INNER join positions p ON p.userId=u.id AND p.organizationId IN (1,2) left join connections c ON c.userId=u.id OR c.connectedUserId=u.id where c.userId is null OR c.connectedUserId is NULL
        /*$queryUser = User::find()->select('user.id,user.username,organizations.name')
                    ->joinWith([
                    'positions' => function ($query) {
                        $query->where(['positions.organizationId' => [1,2]]);
                    },
                ])->joinWith('organization')->asArray()->all();*/
        echo '<pre>'; 
        //$queryUser = User::find()->select('user.id,user.username')->all();
        $queryUser = User::find()
        ->leftJoin('connections', 'connections.userId = user.id OR connections.connectedUserId = user.id')
        ->joinWith([
                    'positions' => function ($query) {
                        $query->where(['positions.organizationId' => [1,2]]); // [1,2] is loggedIn user's current & previous organization ids
                    },
                ])
        ->with('organization')//list all organization linked to user, a separate array containing organization will be there, call getOrganization relation from User Model
        ->with('positions.organization') // list organization related to position/nested listing, call getOrganization relation from Position Model
        //->with('currentOrganization') //list current organization linked to user, call getCurrentOrganization relation from User Model
        
        // current position with organization
        /*->joinWith(['positions'  => function ($query1) {
                        $query1->where(['positions.isCurrent' => 1]);
                    },]) // list organization related to position/nested listing, call getOrganization relation from Position Model
        ->with('positions.organization')*/
        
        ->with('currentOrganization') //list current organization linked to user
        ->where(['is', 'connections.userId', null])
        ->orWhere(['is', 'connections.connectedUserId', null])
        ->asArray()->all();
        foreach ($queryUser as $key => $user) {
            print_r($user);
            //$positions = $user->getPositions()->where(['positions.organizationId' => [1,2]])->asArray()->all();
            echo "<hr>";
            //print_r($positions);
            //$positions = $user->getPositions()->asArray()->all();
            //print_r($positions);

        }
        die();
        //print_r($queryUser);die;            
        //$queryUser->joinWith('connections')->all();

        //echo $queryUser->createCommand()->getRawSql(); exit();
        //echo $queryUser->createCommand()->sql; exit();
        exit;
    }

    /**
     * referral request list
     * list request made by other users to beccome a refferal
     * or we can say friend request list
     * requestStatus=0/1/2 => pending/accepted/rejected
     */
    public function actionReferralRequestList() {
        echo '<pre>';
        $queryUser = User::find()
        ->innerJoin('connections', 'connections.userId = user.id')
        ->joinWith([
                    'positions' => function ($query) {
                        $query->where(['positions.organizationId' => [1,2]]); // [1,2] is loggedIn user's current & previous organization ids
                    },
                ])
        ->with('organization')//list all organization linked to user, a separate array containing organization will be there, call getOrganization relation from User Model
        ->with('positions.organization') // list organization related to position/nested listing, call getOrganization relation from Position Model
        //->with('currentOrganization') //list current organization linked to user, call getCurrentOrganization relation from User Model
        
        // current position with organization
        /*->joinWith(['positions'  => function ($query1) {
                        $query1->where(['positions.isCurrent' => 1]);
                    },]) // list organization related to position/nested listing, call getOrganization relation from Position Model
        ->with('positions.organization')*/
        
        ->with('currentOrganization') //list current organization linked to user
        ->where(['connections.requestStatus' => 0 ]) //requestStatus=0 means request is pending, neither accepted not rejected
        ->andWhere(['connections.connectedUserId' => 3]) // loggedin userId=3
        ->asArray()->all();
        foreach ($queryUser as $key => $user) {
            print_r($user);
            //$positions = $user->getPositions()->where(['positions.organizationId' => [1,2]])->asArray()->all();
            echo "<hr>";
            //print_r($positions);
            //$positions = $user->getPositions()->asArray()->all();
            //print_r($positions);

        }
        die();
        //print_r($queryUser);die;            
        //$queryUser->joinWith('connections')->all();

        //echo $queryUser->createCommand()->getRawSql(); exit();
        //echo $queryUser->createCommand()->sql; exit();
        exit;
    }

    /**
     * referral list
     * list connections/referrals
     * requestStatus=0/1/2 => pending/accepted/rejected
     * used when see connections on Seek Reference
     * used when view user profile and show referrals/connections
     */
    public function actionReferralList() {
        echo '<pre>';
        $queryUser = User::find()
        ->innerJoin('connections', 'connections.userId = user.id')
        ->joinWith([
                    'positions' => function ($query) {
                        $query->where(['positions.organizationId' => [1,2]]); // [1,2] is loggedIn user's current & previous organization ids
                    },
                ])
        ->with('organization')//list all organization linked to user, a separate array containing organization will be there, call getOrganization relation from User Model
        ->with('positions.organization') // list organization related to position/nested listing, call getOrganization relation from Position Model
        //->with('currentOrganization') //list current organization linked to user, call getCurrentOrganization relation from User Model
        
        // current position with organization
        /*->joinWith(['positions'  => function ($query1) {
                        $query1->where(['positions.isCurrent' => 1]);
                    },]) // list organization related to position/nested listing, call getOrganization relation from Position Model
        ->with('positions.organization')*/
        
        ->with('currentOrganization') //list current organization linked to user
        ->where(['connections.requestStatus' => 1 ]) //requestStatus=0 means request is pending, neither accepted not rejected
        ->andWhere(['or',
                    ['connections.userId' => 3],
                    ['connections.connectedUserId' => 3]
                ]) // loggedin userId=3
        ->asArray()->all();
        foreach ($queryUser as $key => $user) {
            print_r($user);
            //$positions = $user->getPositions()->where(['positions.organizationId' => [1,2]])->asArray()->all();
            echo "<hr>";
            //print_r($positions);
            //$positions = $user->getPositions()->asArray()->all();
            //print_r($positions);

        }
        die();
        //print_r($queryUser);die;            
        //$queryUser->joinWith('connections')->all();

        //echo $queryUser->createCommand()->getRawSql(); exit();
        //echo $queryUser->createCommand()->sql; exit();
        exit;
    }
}
