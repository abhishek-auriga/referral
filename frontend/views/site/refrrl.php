<?php
//phpinfo(); exit;
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Login with Linkedin';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    if (!empty($model->getErrorSummary(""))) {
    	echo Html::beginTag('div', ['class' => 'has-error']);
    	foreach($model->getErrorSummary("") as $error) {
    		echo Html::tag('p', $error, ['class' => 'help-block help-block-error']);
    	}
    	echo Html::endTag('div');
    }

    ?>

    <?php
    //$authorizationUrl = 'https://www.linkedin.com/uas/oauth2/authorization';
    $authorizationUrl = 'https://www.linkedin.com/oauth/v2/authorization';

//echo '<a href="' . $authorizationUrl . '?response_type=code&client_id='.Yii::$app->params['Client_ID'].'&redirect_uri='.Yii::$app->params['callback_url'].'&state=98765EeFWf45A53sdfKef4233&scope=r_basicprofile r_emailaddress"><img src="./images/linkedin_connect_button.png" alt="Sign in with LinkedIn"/></a>';
    echo '<a href="' . $authorizationUrl . '?response_type=code&client_id='.Yii::$app->params['Client_ID'].'&redirect_uri='.Yii::$app->params['callback_url'].'&state=98765EeFWf45A53sdfKef4233&scope=r_basicprofile r_emailaddress"><img src="./images/linkedin_connect_button.png" alt="Sign in with LinkedIn"/></a>';
?>
</div>
