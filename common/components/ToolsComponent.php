<?php
namespace common\components;

use Yii;
use yii\base\Component;
use yii\helpers\Html;

class ToolsComponent extends Component{
    public $content;
	
    public function init(){
        parent::init();
        $this->content= 'Hello Yii 2.0';
    }
	
    public function display($content=null){
        if($content!=null){
            $this->content= $content;
        }
        echo Html::encode($this->content);
    }

    public function getAccessToken($code = null) {
        if (!is_null($code)) {
            $url = 'https://www.linkedin.com/uas/oauth2/accessToken';
            //$url = 'https://www.linkedin.com/oauth/v2/accessToken';
            $param = 'grant_type=authorization_code&code=' . $code . '&redirect_uri=' . Yii::$app->params['callback_url'] . '&client_id=' . Yii::$app->params['Client_ID'] . '&client_secret=' . Yii::$app->params['Client_Secret'];
            //$return = (json_decode(post_curl($url,$param),true)); // Request for access token

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $param);

            // in real life you should use something like:
            // curl_setopt($ch, CURLOPT_POSTFIELDS, 
            //          http_build_query(array('postvar1' => 'value1')));

            // receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $server_output = curl_exec ($ch);

            curl_close ($ch);
            return $return = json_decode($server_output, true);
        } else {
            return ['error' => 'Code should not be empty.'];
        }
    }

    public function getUserData($accessToken = null) {
        if(!is_null($accessToken)) {
            $url = 'https://api.linkedin.com/v1/people/~:(id,firstName,lastName,pictureUrls::(original),headline,publicProfileUrl,location,industry,positions,email-address)?format=json&oauth2_access_token='. $accessToken;
            //$User = json_decode(post_curl($url)); // Request user information on received token

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 0);
            //curl_setopt($ch, CURLOPT_POSTFIELDS,"postvar1=value1&postvar2=value2&postvar3=value3");

            // in real life you should use something like:
            // curl_setopt($ch, CURLOPT_POSTFIELDS, 
            //          http_build_query(array('postvar1' => 'value1')));

            // receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec ($ch);

            curl_close ($ch);
            $recordSet = json_decode($response, true);

            return $recordSet;
        } else {
            return ['error' => 'Access Token should not be empty.'];
        }

    }
}
?>