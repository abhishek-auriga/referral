<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "connections".
 *
 * @property int $id
 * @property int $userId
 * @property int $connectedUserId
 * @property int $requestStatus
 * @property int $knowType
 * @property string $location
 * @property string $fromDate
 * @property string $toDate
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Connections extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'connections';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'connectedUserId', 'fromDate', 'toDate', 'created_at', 'updated_at'], 'required'],
            [['userId', 'connectedUserId', 'created_at', 'updated_at'], 'integer'],
            [['fromDate', 'toDate'], 'safe'],
            [['requestStatus'], 'string', 'max' => 2],
            [['knowType', 'status'], 'string', 'max' => 1],
            [['location'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'connectedUserId' => 'Connected User ID',
            'requestStatus' => 'Request Status',
            'knowType' => 'Know Type',
            'location' => 'Location',
            'fromDate' => 'From Date',
            'toDate' => 'To Date',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
